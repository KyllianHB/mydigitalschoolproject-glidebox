'use strict';

/**
 * casier service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::casier.casier');
