'use strict';

/**
 * casier controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::casier.casier');
