'use strict';

/**
 * planche-surf service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::planche-surf.planche-surf');
