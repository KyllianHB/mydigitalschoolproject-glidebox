'use strict';

/**
 * planche-surf controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::planche-surf.planche-surf');
