'use strict';

/**
 * planche-surf router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::planche-surf.planche-surf');
