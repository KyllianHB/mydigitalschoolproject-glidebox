'use strict';

/**
 * check-up controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::check-up.check-up');
