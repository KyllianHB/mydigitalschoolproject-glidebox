'use strict';

/**
 * check-up router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::check-up.check-up');
