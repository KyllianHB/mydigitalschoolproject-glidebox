'use strict';

/**
 * check-up service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::check-up.check-up');
