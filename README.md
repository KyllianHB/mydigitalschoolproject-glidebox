
- Création d'une instance mysql avec Docker : 

    docker run --name mysql-db -e MYSQL_ROOT_PASSWORD=root -d mysql

- Lancement phpmyadmin avec Docker : 

    docker run --name phpmyadmin -d --link mysql-db:db -p 8080:80 -e PMA_HOST=db -e MYSQL_ROOT_PASSWORD=root phpmyadmin