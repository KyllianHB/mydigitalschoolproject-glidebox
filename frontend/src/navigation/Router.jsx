import { Route, Routes } from 'react-router-dom'
import Accueil from '../pages/Accueil'

function Router () {
  return (
    <Routes>
      <Route index path='/' element={<Accueil />} />
    </Routes>
  )
}

export default Router

// https://api.openweathermap.org/data/2.5/weather?lat=47.218371&lon=-1.553621&appid=087df1d16027831de5059b7e472cce91&lang=fr&units=metric&dt=1683019841