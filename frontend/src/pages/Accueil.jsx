import { useState, useEffect } from 'react'
import { MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'
import { getPlanches } from '../services/Api'
import Header from '../components/Header'
import SwitchMap from '../components/SwitchMap'
import Footer from '../components/Footer';
import '../styles/Accueil.scss';
import React from 'react'
import InstagramFeed  from 'react-ig-feed'
import 'react-ig-feed/dist/index.css'
import ReactWeather, { useWeatherBit } from 'react-open-weather';
import { Icon } from 'leaflet'
import { Button } from '@mui/material'
import CircleIcon from '@mui/icons-material/Circle';
function Accueil () {
  const [planches, setPlanches] = useState()

  useEffect(() => {
    const getData = async () => {
      const result = await getPlanches()
      console.log(result)
      setPlanches(result)
    }
    getData()
  }, [])


const { data, isLoading, errorMessage } = useWeatherBit({
  key: 'b285cad21eb64656b2522fda6ab3a9c5',
  lat: '47.218371',
  lon: '-1.553621',
  lang: 'fr',
  unit: 'M', // values are (M,S,I)
});

const customIcon = new Icon({
  iconUrl: 'pin.svg',
  iconSize: [25,41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
});

  if (!planches) {
    return <h1>Chargement...</h1>
  }
  
  return (
    <>
      <Header />
      
      <div className='container'>  
        <section className='section-bienvenu'>
          <h3>Bienvenue sur Glide Box ! 👋</h3>
          <div>
            <p>Glide Box c’est quoi ? 🧐</p>
            <p>C’est un système de location de planches de surf 24h/24 et 7j/7 
              via notre application Glide Box. Il vous suffit de trouver 
              un locker à proximité et d’y déverrouiller la planche que 
              vous souhaitez sur l’application. Ensuite, 
              il ne vous reste plus qu’à profiter à 
              fond de votre session de surf ! 🏄</p>
          </div>
        </section>
        <section className='section-carte'>
          <h3>Carte des lockers</h3>
          <div className='carte'>
            <MapContainer center={[47.2186371, -1.5541362]} zoom={8} scrollWheelZoom={false}>
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
              <Marker position={[47.190335588827935, -2.1617256335115065]} icon={customIcon}>
                <Popup>
                  <div className='popup'>
                    <div className='info-co'>
                      <div className='pulsating-circle'></div>
                      <span>En ligne</span>
                    </div>
                    <span>planche(s) de disponible : 5</span>
                    <img src="box.png" alt="" width="100px" />
                    <Button 
                    onClick={() => {alert('clicked');}} 
                    variant="contained"
                    className='buttonBox'
                    sx={{
                    }}
                    >
                      <span>Let's go !</span>
                      <span>🌊</span>
                    </Button>
                  </div>
                </Popup>
              </Marker>
              <Marker position={[47.26495922563043, -2.453689067126801]} icon={customIcon}>
                <Popup>
                  <div className='popup'>
                    <div className='info-co'>
                      <div className='pulsating-circle'></div>
                      <span>En ligne</span>
                    </div>
                    <span>planche(s) de disponible : 5</span>
                    <img src="box.png" alt="" width="100px" />
                    <Button 
                    onClick={() => {alert('clicked');}} 
                    variant="contained"
                    className='buttonBox'
                    sx={{
                    }}
                    >
                      <span>Let's go !</span>
                      <span>🌊</span>
                    </Button>
                  </div>
                </Popup>
              </Marker>
            </MapContainer>
          </div>
          <SwitchMap />
        </section>

        <section>
          <h3>Meteo en temps réel</h3>
          {window.location.host !== 'localhost:3001' ? (
            <>
              <ReactWeather
                isLoading={isLoading}
                errorMessage={errorMessage}
                data={data}
                lang="en"
                locationLabel="Nantes"
                unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
                showForecast
              />
            </>
            ) : (
              <p>Mode dev</p>
          )}
        </section>



        <section>
          <h3>Réseaux sociaux</h3>
          <InstagramFeed token="IGQVJVV291WkdQTlZAWYV85dU94b0c4ajRqb2YzZAGhrR3FfZAnlaOUhOcHBCQ3JNSkpZAdDFuTDJJWHpWZAzBhZAFNKY1FBOTJIQVFYVTBtTW01S0YxS0lxeTZAFVUl4VUU1MWJhY21Pb1RDS1RtZAjJUOG1HbAZDZD"  counter="6"/>  
        </section>

      </div>

      <Footer />
    </>
  )
}

export default Accueil
