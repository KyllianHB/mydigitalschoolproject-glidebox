// import './styles/App.scss'
// import Navbar from './navigation/Navbar'
import Router from './navigation/Router'
import './styles/App.scss';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';



function App () {
  return (
    
    <div className='App'>
      <link
        rel="stylesheet"
        href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossOrigin=""
      />
      {/* <link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/react-open-weather/1.2.1/react-open-weather.min.css" 
        integrity="sha512-i7GLa0ZqgNjvEweHIDggxNO1IlktIYn53eVuxIbsXJmu8MFbqZPJlMhx+oAaJvDT8M83TAX1/ubAkkhdas/b9w==" 
        crossOrigin="anonymous" 
        referrerPolicy="no-referrer" 
      /> */}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/react-open-weather/1.2.1/react-open-weather.min.css" />

      
      <Router />
    </div>
  )
}

export default App
