import { useNavigate } from 'react-router-dom'
// import '../styles/FooList.scss'

function FooList ({ restaurant }) {
  const { attributes } = restaurant
  const navigate = useNavigate()
  // => const attributes = restaurant.attributes
  const handleClick = (restaurant) => {
    navigate(`/restaurant/${restaurant.attributes.slug}`, { state: { id: restaurant.id } })
  }
  return (
    <div className='card'>
        <p>{attributes.description.substring(0, 180)}...</p>
    </div>
  )
}

export default FooList
