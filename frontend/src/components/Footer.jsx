import { useState, useEffect } from 'react'
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import SurfingOutlinedIcon from '@mui/icons-material/SurfingOutlined';
import CottageOutlinedIcon from '@mui/icons-material/CottageOutlined';
import { SvgIcon } from '@mui/material';

export default function Footer() {
  const [value, setValue] = useState('recents');

  useEffect(() => {
    console.log(window)
    switch (window.location.pathname) {
      case '/':
        setValue('accueil')
      break;
  
      default:
        setValue('accueil')
      break;
    }
  }, [])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const PlancheIcon = (props) => {
    // const { active, ...otherProps } = props;

    return (
      <SvgIcon {...props} >
        <svg width="23.872" height="23.872" viewBox="0 0 23.872 23.872">
          <defs>
            <clipPath id="clip-path">
              <rect id="Rectangle_30" data-name="Rectangle 30" width="23.872" height="23.872" />
            </clipPath>
          </defs>
          <g id="Groupe_61" data-name="Groupe 61" transform="translate(0 0)" clipPath="url(#clip-path)">
            <path id="Tracé_26" data-name="Tracé 26" d="M4.122,23.872a1.2,1.2,0,0,1-1.193-1.2V20.944H1.2A1.194,1.194,0,0,1,.128,19.215,39.015,39.015,0,0,1,7.385,9.4C12.456,4.328,18.442.816,23.4.006a.408.408,0,0,1,.469.469c-.811,4.955-4.323,10.941-9.394,16.013a39.041,39.041,0,0,1-9.815,7.257,1.185,1.185,0,0,1-.535.128M22.955.916c-4.678.952-10.234,4.3-14.993,9.061a38.21,38.21,0,0,0-7.105,9.6.37.37,0,0,0,.016.364.374.374,0,0,0,.324.181h2.14a.408.408,0,0,1,.408.408v2.14A.374.374,0,0,0,3.926,23a.368.368,0,0,0,.364.016,38.221,38.221,0,0,0,9.605-7.105C18.654,11.151,22,5.595,22.955.916" transform="translate(0 0)" />
            <path id="Tracé_27" data-name="Tracé 27" d="M4.686,10.589H4.652a.407.407,0,0,1-.3-.178A25.818,25.818,0,0,1,.005.474.408.408,0,0,1,.474,0,26.087,26.087,0,0,1,10.683,4.54a.408.408,0,1,1-.464.671A26.515,26.515,0,0,0,.916.916,25.369,25.369,0,0,0,4.743,9.532q.327-.339.662-.675a.408.408,0,0,1,.577.577c-.34.341-.673.684-1,1.028a.407.407,0,0,1-.295.126" transform="translate(0 0)" />
            <path id="Tracé_28" data-name="Tracé 28" d="M44.919,48.5a1.185,1.185,0,0,1-.535-.128,35.817,35.817,0,0,1-6.094-3.939.408.408,0,0,1-.026-.617c.5-.467.935-.891,1.342-1.3a.408.408,0,0,1,.577.577c-.316.316-.65.642-1.015.989a34.66,34.66,0,0,0,5.583,3.56.369.369,0,0,0,.364-.016A.376.376,0,0,0,45.3,47.3V45.16a.408.408,0,0,1,.408-.408h2.14a.374.374,0,0,0,.324-.181.369.369,0,0,0,.016-.364,35.269,35.269,0,0,0-4.075-6.232.408.408,0,0,1,.635-.512,36.12,36.12,0,0,1,4.168,6.377,1.194,1.194,0,0,1-1.069,1.728H46.113V47.3a1.2,1.2,0,0,1-1.193,1.2" transform="translate(-25.17 -24.624)" />
            <path id="Tracé_29" data-name="Tracé 29" d="M5.414,28.065a.408.408,0,0,1-.344-.627A40.839,40.839,0,0,1,10.2,20.993a.41.41,0,0,1,.163-.1A23.947,23.947,0,0,1,15.192,19.8a27.287,27.287,0,0,0,9.474-2.958.408.408,0,0,1,.556.54,35.027,35.027,0,0,1-3.754,5.99.412.412,0,0,1-.162.126,27.867,27.867,0,0,1-7.827,2.125,23.783,23.783,0,0,0-7.881,2.4.406.406,0,0,1-.185.044m5.3-6.432a40.846,40.846,0,0,0-4.15,5.034,24.4,24.4,0,0,1,6.822-1.858,27.039,27.039,0,0,0,7.5-2.022,35.316,35.316,0,0,0,3.042-4.671,27.906,27.906,0,0,1-8.64,2.491,23.083,23.083,0,0,0-4.574,1.026" transform="translate(-3.304 -11.083)" />
          </g>
        </svg>
      </SvgIcon>
    );
  }
  


  return (
    <BottomNavigation sx={{
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 98
      }} 
      value={value} 
      onChange={handleChange}
      defaultValue="accueil" 
    >
      
      <BottomNavigationAction
        label="Accueil"
        value="accueil"
        icon={<CottageOutlinedIcon />}
        sx={{
          '&.Mui-selected > *': {
            fill: '#eb600c',
          },
          '&:not(.Mui-selected) > *': {
            fill: '#27357A',
          },
          '&.Mui-selected >  .MuiBottomNavigationAction-label': {
            color: '#eb600c',
          }
        }}
      />
      <BottomNavigationAction
        label="Planches"
        value="planches"
        icon={<PlancheIcon />}
        sx={{
          '&.Mui-selected > *': {
            fill: '#eb600c',
          },
          '&:not(.Mui-selected) > *': {
            fill: '#27357A',
          },
          '&.Mui-selected >  .MuiBottomNavigationAction-label': {
            color: '#eb600c',
          }
        }}
      />
      <BottomNavigationAction
        label="Ma location"
        value="maLocation"
        icon={<SurfingOutlinedIcon />}
        sx={{
          '&.Mui-selected > *': {
            fill: '#eb600c',
          },
          '&:not(.Mui-selected) > *': {
            fill: '#27357A',
          },
          '&.Mui-selected > .MuiBottomNavigationAction-label': {
            color: '#eb600c',
          }
          
        }}
      />
    </BottomNavigation>
  );
}