import { Switch } from '@mui/material';
import '../styles/SwitchMap.scss'
import {CameraAlt, Map} from '@mui/icons-material'


function SwitchMap () {
    return ( 
        <div className="div-switchMap" >
            <Map/>
            <Switch color="warning" />
            <CameraAlt />
      </div>
     );
}

export default SwitchMap;