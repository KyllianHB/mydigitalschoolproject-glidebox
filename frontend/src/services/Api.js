import axios from 'axios'

const api = axios.create({
  // baseURL: 'http://localhost:1337/api/',
  baseURL: 'http://164.90.172.153:1337/api/',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  },
  timeout: 10000
})

  const getPlanches = async () => {
    try {
      const response = await api.get('/planche-surfs?populate=*')
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

export {
  getPlanches
}